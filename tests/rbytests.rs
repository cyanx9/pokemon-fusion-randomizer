#[cfg(test)]

use pokemon_fusion_randomizer::engine::rom_manager::*;
use pokemon_fusion_randomizer::engine::log::*;
use std::path::Path;
use std::fs::File;

#[test]
fn read_names_correctly_start() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_names(&mut file) {
		Err(why) => panic!("Reading names from rom failed! {}", why),
		Ok(result) => result,
	};
	let rhydon_read = &read[0..10];
	let rhydon = [0x91, 0x87, 0x98, 0x83, 0x8E, 0x8D, 0x50, 0x50, 0x50, 0x50];
	assert_eq!(rhydon_read, rhydon);
}

#[test]
fn read_names_correctly_middle() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_names(&mut file) {
		Err(why) => panic!("Reading names from rom failed! {}", why),
		Ok(result) => result,
	};
	let ditto_read = &read[750..760];
	let ditto = [0x83, 0x88, 0x93, 0x93, 0x8E, 0x50, 0x50, 0x50, 0x50, 0x50];
	assert_eq!(ditto_read, ditto);
}

#[test]
fn read_names_correctly_end()  {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_names(&mut file) {
		Err(why) => panic!("Reading names from rom failed! {}", why),
		Ok(result) => result,
	};
	let victreebel_read = &read[read.len()-10..read.len()];
	let victreebel = [0x95, 0x88, 0x82, 0x93, 0x91, 0x84, 0x84, 0x81, 0x84, 0x8B];
	assert_eq!(victreebel_read, victreebel);
}

#[test]
fn read_base_data_correctly_start() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_base_data(&mut file) {
	Err(why) => panic!("Reading base data from rom failed: {}", why),
	Ok(read) => read,
	};
	let bulbasaur_read = &read[0..28];
	let bulbasaur = [0x01, 0x2D, 0x31, 0x31, 0x2D,
					 0x41, 0x16, 0x03, 0x2D, 0x40,
					 0x55, 0x00, 0x40, 0xE5, 0x40,
					 0x21, 0x2D, 0x00, 0x00, 0x03,
					 0xA4, 0x03, 0x38, 0xC0, 0x03,
					 0x08, 0x06, 0x00];
	assert_eq!(bulbasaur_read, bulbasaur);
}


#[test]
fn read_base_data_correctly_middle() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_base_data(&mut file) {
	Err(why) => panic!("Reading base data from rom failed: {}", why),
	Ok(read) => read,
	};
	let slowpoke_read = &read[78*28..79*28];
	let slowpoke = [0x4F, 0x5A, 0x41, 0x41, 0x0F,
				  0x28, 0x15, 0x18, 0xBE, 0x63,
				  0x55, 0xA0, 0x46, 0xC2, 0x47,
				  0x5D, 0x00, 0x00, 0x00, 0x00,
				  0xA0, 0xBF, 0x08, 0xFE, 0xE3,
				  0x38, 0x73, 0x00];
	assert_eq!(slowpoke_read, slowpoke);
}

#[test]
fn read_base_data_correctly_end() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_base_data(&mut file) {
	Err(why) => panic!("Reading base data from rom failed: {}", why),
	Ok(read) => read,
	};
	let mew_read = &read[150*28..read.len()];
	let mew = [0x97, 0x64, 0x64, 0x64, 0x64,
			   0x64, 0x18, 0x18, 0x2D, 0x40,
			   0x55, 0x12, 0x41, 0x05, 0x42,
			   0x01, 0x00, 0x00, 0x00, 0x03,
			   0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
			   0xFF, 0xFF, 0xFF];
	assert_eq!(mew_read, mew);
}

#[test]
fn read_palette_data_correctly_start() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_palette_data(&mut file) {
	Err(why) => panic!("Reading palette data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read10 = &read[0..10];
	let palette10 = [0x16, 0x16, 0x16,
					 0x12, 0x12, 0x12,
					 0x13, 0x13, 0x13,
					 0x16];
	assert_eq!(read10, palette10);
}

#[test]
fn read_palette_data_correctly_middle() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_palette_data(&mut file) {
	Err(why) => panic!("Reading palette data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read10 = &read[50..60];
	let palette10 = [0x15, 0x18, 0x18,
					 0x18, 0x13, 0x15,
					 0x15, 0x15, 0x12,
					 0x11];
	assert_eq!(read10, palette10);
}

#[test]
fn read_palette_data_correctly_end() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_palette_data(&mut file) {
	Err(why) => panic!("Reading palette data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read10 = &read[read.len()-10..read.len()];
	let palette10 = [0x19, 0x17, 0x11,
					 0x18, 0x12, 0x19,
					 0x11, 0x15, 0x10,
					 0x10];
	assert_eq!(read10, palette10);
}

#[test]
fn read_cries_correctly_start() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_cry_data(&mut file) {
	Err(why) => panic!("Reading cry data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read9 = &read[0..9];
	let cry9 = [0x11, 0x00, 0x80,
				0x03, 0x00, 0x80,
				0x00, 0x00, 0x80];
	assert_eq!(read9, cry9);
}

#[test]
fn read_cries_correctly_middle() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_cry_data(&mut file) {
	Err(why) => panic!("Reading cry data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read9 = &read[87..96];
	let cry9 = [0x12, 0x00, 0x80,
				0x00, 0x00, 0x00,
				0x00, 0x00, 0x00];
	assert_eq!(read9, cry9);
}

#[test]
fn read_cries_correctly_end() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();
	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open file {}: {}", display, why),
		Ok(file) => file,
	};
	let read = match read_cry_data(&mut file) {
	Err(why) => panic!("Reading cry data from rom failed: {}", why),
	Ok(read) => read,
	};
	let read9 = &read[read.len()-9..read.len()];
	let cry9 = [0x21, 0x55, 0x01,
				0x25, 0x44, 0x20,
				0x25, 0x66, 0xCC];
	assert_eq!(read9, cry9);
}

#[test]
fn decode_names_correctly_1() {
	let name = [0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89];
	assert_eq!(String::from("ABCDEFGHIJ"), name_decoder(name));
}

#[test]
fn decode_names_correctly_2() {
	let name = [0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93];
	assert_eq!(String::from("KLMNOPQRST"), name_decoder(name));
}

#[test]
fn decode_names_correctly_3() {
	let name = [0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0xE8, 0xEF, 0xF5, 0xE0];
	assert_eq!(String::from("UVWXYZ.\u{2642}\u{2640}\'"), name_decoder(name));
}

#[test]
fn decode_names_correctly_4() {
	let name = [0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50];
	assert_eq!(String::from("          "), name_decoder(name));
}

#[cfg(test)]
use std::fs::File;
use std::path::Path;
use std::io::SeekFrom;
use std::io::prelude::*;

#[test]
fn file_exists() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();

	let mut _file = match File::open(&path) {
		Err(why) => panic!("Could not open {}: {}", display, why),
		Ok(file) => file,
	};
}

#[test]
fn read_rhydon() {
	let path = Path::new("pokered-fusion/pokered.gbc");
	let display = path.display();

	let mut file = match File::open(&path) {
		Err(why) => panic!("Could not open {}: {}", display, why),
		Ok(file) => file,
	};
	
	let mut buffer = [0; 10];
	match file.seek(SeekFrom::Start(0x1C21E)) {
		Err(why) => panic!("Could not seek in file {}:{}", display, why),
		Ok(n) => assert_eq!(n, 0x1C21E),
	};
	match file.read(&mut buffer) {
		Err(why) => panic!("Could not read from file {}: {}", display, why),
		Ok(n) => assert_eq!(n, 10),
	};

	let rhydon = [0x91, 0x87, 0x98, 0x83, 0x8E, 0x8D, 0x50, 0x50, 0x50, 0x50];
	assert_eq!(buffer, rhydon);
}
// TODO: tests for
//  base data address: 0x383DE
//  base data of mew address: 0x0425B
//  cries data address: 0x39446
//  palettes data address: 0x725C9

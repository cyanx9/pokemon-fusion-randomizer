use crate::engine::constants::*;

pub struct Pokemon {
	name: [u8;RB_NAME_LENGTH],
	base_data: [u8;RB_BASE_DATA_LENGTH],
	palette: u8,
	cry: [u8;RB_CRY_DATA_LENGTH],
}

impl Pokemon {
	pub fn get_name(&self) -> [u8;RB_NAME_LENGTH] {
		self.name
	}

	pub fn get_base_data(&self) ->[u8;RB_BASE_DATA_LENGTH] {
		self.base_data
	}

	pub fn get_palette(&self) -> u8 {
		self.palette
	}

	pub fn get_cry(&self) -> [u8;RB_CRY_DATA_LENGTH] {
		self.cry
	}
	
	pub fn new(name_in: &[u8], base_data_in: &[u8], palette_data: &u8, cry_data: &[u8]) -> Self {
		Self {
			name: name_in.try_into().expect("NAME: slice was incorrect length"),
			base_data: base_data_in.try_into().expect("BASE DATA: slice was incorrect length"),
			palette: *palette_data,
			cry: cry_data.try_into().expect("CRY: slice was incorrect length"),
		}
	}

	fn fuse(&self, fusion_mon: &Pokemon) -> Pokemon {
		Pokemon::new(
			&self.fuse_name(fusion_mon),
			&self.fuse_base_data(fusion_mon),
			&self.fuse_palettes(fusion_mon),
			&self.cry //&self.fuse_cries(fusion_mon)
				)
	}
	
	fn fuse_name(&self, fusion_mon: &Pokemon) -> [u8;10] {
		let mut fusion_name = [0x50;10];
		let mut first_name = Vec::new();
		for l in self.name {
			if l != 0x50 {
				first_name.push(l);
			}
		}
		let mut second_name = Vec::new();
		for l in fusion_mon.name {
			if l!= 0x50 {
				second_name.push(l);
			}
		}
		let mut first_part = Vec::new();
		let mut second_part = Vec::new();
		for i in 0..5 {
			if i < first_name.len() && i < second_name.len() {
				if first_name[i] == second_name[second_name.len()-1-i] && i > 1{
					first_part.push(first_name[i]);
					break;
				}
				if i < first_name.len() {
					first_part.push(first_name[i]);
				}
				if i < second_name.len() {
					second_part.push(second_name[second_name.len()-1-i])
				}
			}
		}
		for i in 0..first_part.len() {
			fusion_name[i] = first_part[i];
		}
		for i in 0..second_part.len() {
			fusion_name[i+first_part.len()] = second_part[second_part.len()-1-i];
		}
		fusion_name
	}

	fn fuse_base_data(&self, fusion_mon: &Pokemon) -> [u8;RB_BASE_DATA_LENGTH] {
		let base_stats = self.fuse_base_stats(fusion_mon);
		let types = self.fuse_types(fusion_mon);
		let catch_rate = self.fuse_catch_rate(fusion_mon);
		let base_exp = self.fuse_base_exp(fusion_mon);
		let lvl1_attacks = self.fuse_lvl1_attacks(fusion_mon);
		let growth_rate = self.fuse_growth_rate(fusion_mon);
		let tm_hm_flags = self.fuse_tm_hm_flags(fusion_mon);
		let mut base_data = self.base_data.clone();
		base_data[1] = base_stats[0];
		base_data[2] = base_stats[1];
		base_data[3] = base_stats[2];
		base_data[4] = base_stats[3];
		base_data[5] = base_stats[4];
		base_data[6] = types[0];
		base_data[7] = types[1];
		base_data[8] = catch_rate;
		base_data[9] = base_exp;
		base_data[15] = lvl1_attacks[0];
		base_data[16] = lvl1_attacks[1];
		base_data[17] = lvl1_attacks[2];
		base_data[18] = lvl1_attacks[3];
		base_data[19] = growth_rate;
		base_data[20] = tm_hm_flags[0];
		base_data[21] = tm_hm_flags[1];
		base_data[22] = tm_hm_flags[2];
		base_data[23] = tm_hm_flags[3];
		base_data[24] = tm_hm_flags[4];
		base_data[25] = tm_hm_flags[5];
		base_data[26] = tm_hm_flags[6];
		base_data
	}

	fn fuse_base_stats(&self, fusion_mon: &Pokemon) -> [u8;5] {
		let mut base_stats = [0;5];
		base_stats[0] = ((self.base_data[1] as u16 + fusion_mon.base_data[1] as u16) / 2).try_into().unwrap();
		base_stats[1] = ((self.base_data[2] as u16 + fusion_mon.base_data[2] as u16) / 2).try_into().unwrap();
		base_stats[2] = ((self.base_data[3] as u16 + fusion_mon.base_data[3] as u16) / 2).try_into().unwrap();
		base_stats[3] = ((self.base_data[4] as u16 + fusion_mon.base_data[4] as u16) / 2).try_into().unwrap();
		base_stats[4] = ((self.base_data[5] as u16 + fusion_mon.base_data[5] as u16) / 2).try_into().unwrap();
		base_stats
	}

	fn fuse_types(&self, fusion_mon: &Pokemon) -> [u8;2] {
		let mut types = [0;2];
		types[0] = self.base_data[6];
		types[1] = fusion_mon.base_data[6];
		types
	}

	fn fuse_catch_rate(&self, fusion_mon: &Pokemon) -> u8 {
		((self.base_data[8] as u16 + fusion_mon.base_data[8] as u16) / 2).try_into().unwrap()
	}

	fn fuse_base_exp(&self, fusion_mon: &Pokemon) -> u8 {
		((self.base_data[9] as u16 + fusion_mon.base_data[9] as u16) / 2).try_into().unwrap()
	}

	fn fuse_lvl1_attacks(&self, fusion_mon: &Pokemon) -> [u8;4] {
		let mut lvl1_attacks = [0;4];
		let mut i = 0;
		let mut j = 0;
		let mut k = 0;
		while i < 4 {
			if j <= k && self.base_data[15+j] != 0 {
				lvl1_attacks[i] = self.base_data[15+j];
				i = i + 1;
				j = j + 1;
			}
			else if k < j && fusion_mon.base_data[15+k] != 0 {
				lvl1_attacks[i] = fusion_mon.base_data[15+k];
				i = i + 1;
				k = k + 1;
			}
			else {
				i = i + 1;
			}
		}
			
		lvl1_attacks
	}

	fn fuse_growth_rate(&self, fusion_mon: &Pokemon) -> u8 {
		match self.base_data[19] {
			0 => {
				match fusion_mon.base_data[19] {
					0 => 0,
					3 => 0,
					4 => 3,
					5 => 3,
					_ => panic!("Growth rate invalid"),
				}
			},
			3 => {
				match fusion_mon.base_data[19] {
					0 => 3,
					3 => 3,
					4 => 3,
					5 => 4,
					_ => panic!("Growth rate invalid"),
				}
			},
			4 => {
				match fusion_mon.base_data[19] {
					0 => 3,
					3 => 4,
					4 => 4,
					5 => 4,
					_ => panic!("Growth rate invalid"),
				}
			},
			5 => {
				match fusion_mon.base_data[19] {
					0 => 4,
					3 => 4,
					4 => 5,
					5 => 5,
					_ => panic!("Growth rate invalid"),
				}
			},
			_ => panic!("Growth rate invalid"),
		}
	}

	fn fuse_tm_hm_flags(&self, fusion_mon: &Pokemon) -> [u8;7] {
		let mut tm_hm_flags = [0;7];
		tm_hm_flags[0] = self.base_data[20] | fusion_mon.base_data[20];
		tm_hm_flags[1] = self.base_data[21] | fusion_mon.base_data[21];
		tm_hm_flags[2] = self.base_data[22] | fusion_mon.base_data[22];
		tm_hm_flags[3] = self.base_data[23] | fusion_mon.base_data[23];
		tm_hm_flags[4] = self.base_data[24] | fusion_mon.base_data[24];
		tm_hm_flags[5] = self.base_data[25] | fusion_mon.base_data[25];
		tm_hm_flags[6] = self.base_data[26] | fusion_mon.base_data[26];
		tm_hm_flags
	}

	fn fuse_palettes(&self, fusion_mon: &Pokemon) -> u8 {
		fusion_mon.palette
	}
}

fn get_indexnum(pokedex: u8) -> usize {
	match pokedex {
		1 => 0x99,
		2 => 0x09,
		3 => 0x9A,
		4 => 0xB0,
		5 => 0xB2,
		6 => 0xB4,
		7 => 0xB1,
		8 => 0xB3,
		9 => 0x1C,
		10 => 0x7B,
		11 => 0x7C,
		12 => 0x7D,
		13 => 0x70,
		14 => 0x71,
		15 => 0x72,
		16 => 0x24,
		17 => 0x96,
		18 => 0x97,
		19 => 0xA5,
		20 => 0xA6,
		21 => 0x05,
		22 => 0x23,
		23 => 0x6C,
		24 => 0x2D,
		25 => 0x54,
		26 => 0x55,
		27 => 0x60,
		28 => 0x61,
		29 => 0x0F,
		30 => 0xA8,
		31 => 0x10,
		32 => 0x03,
		33 => 0xA7,
		34 => 0x07,
		35 => 0x04,
		36 => 0x8E,
		37 => 0x52,
		38 => 0x53,
		39 => 0x64,
		40 => 0x65,
		41 => 0x6B,
		42 => 0x82,
		43 => 0xB9,
		44 => 0xBA,
		45 => 0xBB,
		46 => 0x6D,
		47 => 0x2E,
		48 => 0x41,
		49 => 0x77,
		50 => 0x3B,
		51 => 0x76,
		52 => 0x4D,
		53 => 0x90,
		54 => 0x2F,
		55 => 0x80,
		56 => 0x39,
		57 => 0x75,
		58 => 0x21,
		59 => 0x14,
		60 => 0x47,
		61 => 0x6E,
		62 => 0x6F,
		63 => 0x94,
		64 => 0x26,
		65 => 0x95,
		66 => 0x6A,
		67 => 0x29,
		68 => 0x7E,
		69 => 0xBC,
		70 => 0xBD,
		71 => 0xBE,
		72 => 0x18,
		73 => 0x9B,
		74 => 0xA9,
		75 => 0x27,
		76 => 0x31,
		77 => 0xA3,
		78 => 0xA4,
		79 => 0x25,
		80 => 0x08,
		81 => 0xAD,
		82 => 0x36,
		83 => 0x40,
		84 => 0x46,
		85 => 0x74,
		86 => 0x3A,
		87 => 0x78,
		88 => 0x0D,
		89 => 0x88,
		90 => 0x17,
		91 => 0x8B,
		92 => 0x19,
		93 => 0x93,
		94 => 0x0E,
		95 => 0x22,
		96 => 0x30,
		97 => 0x81,
		98 => 0x4E,
		99 => 0x8A,
		100 => 0x06,
		101 => 0x8D,
		102 => 0x0C,
		103 => 0x0A,
		104 => 0x11,
		105 => 0x91,
		106 => 0x2B,
		107 => 0x2C,
		108 => 0x0B,
		109 => 0x37,
		110 => 0x8F,
		111 => 0x12,
		112 => 0x01,
		113 => 0x28,
		114 => 0x1E,
		115 => 0x02,
		116 => 0x5C,
		117 => 0x5D,
		118 => 0x9D,
		119 => 0x9E,
		120 => 0x1B,
		121 => 0x98,
		122 => 0x2A,
		123 => 0x1A,
		124 => 0x48,
		125 => 0x35,
		126 => 0x33,
		127 => 0x1D,
		128 => 0x3C,
		129 => 0x85,
		130 => 0x16,
		131 => 0x13,
		132 => 0x4C,
		133 => 0x66,
		134 => 0x69,
		135 => 0x68,
		136 => 0x67,
		137 => 0xAA,
		138 => 0x62,
		139 => 0x63,
		140 => 0x5A,
		141 => 0x5B,
		142 => 0xAB,
		143 => 0x84,
		144 => 0x4A,
		145 => 0x4B,
		146 => 0x49,
		147 => 0x58,
		148 => 0x59,
		149 => 0x42,
		150 => 0x83,
		151 => 0x15,
		_ => panic!("Pokedex number non-existant"),
	}
}

pub fn create_pokemon(names: [u8;RB_INDEXNUM*RB_NAME_LENGTH], base_data: [u8;RB_POKENUM*RB_BASE_DATA_LENGTH], palette_data: [u8;RB_POKENUM], cry_data: [u8;RB_INDEXNUM*RB_CRY_DATA_LENGTH]) -> Vec<Pokemon> {
	let mut pokemon = Vec::new();
	for i in 1..(RB_POKENUM+1) {
		let index = get_indexnum(i.try_into().unwrap());
		pokemon.push(
			Pokemon::new(&names[(index-1)*RB_NAME_LENGTH..index*RB_NAME_LENGTH],
						 &base_data[(i-1)*RB_BASE_DATA_LENGTH..i*RB_BASE_DATA_LENGTH],
						 &palette_data[i-1],
						 &cry_data[(index-1)*RB_CRY_DATA_LENGTH..index*RB_CRY_DATA_LENGTH]
			)
		);
	}
	pokemon
}

pub fn fuse_all(pokemon: &Vec<Pokemon>, fusion_index: &Vec<usize>) -> Vec<Pokemon> {
	let mut fusions = Vec::new();
	for i in 0..RB_POKENUM {
		fusions.push(pokemon[i].fuse(&pokemon[fusion_index[i]]));
	}
	fusions
}

pub fn extract_names(pokemon: &Vec<Pokemon>) -> [u8;RB_INDEXNUM*RB_NAME_LENGTH] {
	let mut names = [0x80;RB_INDEXNUM*RB_NAME_LENGTH];
	for i in 1..RB_POKENUM+1 {
		let n = pokemon[i-1].get_name();
		for j in 0..RB_NAME_LENGTH {
			names[((get_indexnum(i.try_into().unwrap())-1) * RB_NAME_LENGTH) + j] = n[j];
		}
	}

	names
}

pub fn extract_base_data_no_mew(pokemon: &Vec<Pokemon>) -> [u8;(RB_POKENUM-1)*RB_BASE_DATA_LENGTH] {
	let mut base_data = [0;(RB_POKENUM-1)*RB_BASE_DATA_LENGTH];
	for i in 0..RB_POKENUM-1 {
		let b = pokemon[i].get_base_data();
		for j in 0..RB_BASE_DATA_LENGTH {
			base_data[(i*RB_BASE_DATA_LENGTH) +j] = b[j];
		}
	}
	base_data
}

pub fn extract_palettes(pokemon: &Vec<Pokemon>) -> [u8;RB_POKENUM] {
	let mut palettes = [0;RB_POKENUM];
	for i in 0..RB_POKENUM {
		palettes[i] = pokemon[i].get_palette();
	}
	palettes
}


pub fn extract_fusion_numbers(fusion_index: &Vec<usize>) -> [u8;RB_INDEXNUM] {
	let mut fusion_numbers = [0;RB_INDEXNUM];
	for i in 1..RB_POKENUM+1 {
		fusion_numbers[get_indexnum(i.try_into().unwrap())-1] = (get_indexnum((fusion_index[i-1]+1) as u8)) as u8;
	}
	fusion_numbers
}


#[derive(Clone)]
pub enum EvolutionLines {
	One(usize),
	Two(usize, usize),
	Three(usize, usize, usize),
	Eevee(usize, usize, usize, usize),
}

pub const EVOLUTION_LINES: [EvolutionLines;RB_EVO_LINES_NUMBER] = [
	EvolutionLines::Three(0, 1, 2), // Bulbasaur
	EvolutionLines::Three(3, 4, 5), // Charmander
	EvolutionLines::Three(6, 7, 8), // Squirtle
	EvolutionLines::Three(9, 10, 11), // Caterpie
	EvolutionLines::Three(12, 13, 14), // Weedle
	EvolutionLines::Three(15, 16, 17), // Pidgey
	EvolutionLines::Two(18, 19), // Rattata
	EvolutionLines::Two(20, 21), // Spearow
	EvolutionLines::Two(22, 23), // Ekans
	EvolutionLines::Two(24, 25), // Pikachu
	EvolutionLines::Two(26, 27), // Sandshrew
	EvolutionLines::Three(28, 29, 30), // NidoranF
	EvolutionLines::Three(31, 32, 33), // NidoranM
	EvolutionLines::Two(34, 35), // Clefairy
	EvolutionLines::Two(36, 37), // Vulpix
	EvolutionLines::Two(38, 39), // Jigglypuff
	EvolutionLines::Two(40, 41), // Zubat
	EvolutionLines::Three(42, 43, 44), // Oddish
	EvolutionLines::Two(45, 46), // Paras
	EvolutionLines::Two(47, 48), // Venonat
	EvolutionLines::Two(49, 50), // Diglett
	EvolutionLines::Two(51, 52), // Meowth
	EvolutionLines::Two(53, 54), // Psyduck
	EvolutionLines::Two(55, 56), // Mankey
	EvolutionLines::Two(57, 58), // Growlithe
	EvolutionLines::Three(59, 60, 61), // Poliwag
	EvolutionLines::Three(62, 63, 64), // Abra
	EvolutionLines::Three(65, 66, 67), // Machop
	EvolutionLines::Three(68, 69, 70), // Bellsprout
	EvolutionLines::Two(71, 72), // Tentacool
	EvolutionLines::Three(73, 74, 75), // Geodude
	EvolutionLines::Two(76, 77), // Ponyta
	EvolutionLines::Two(78, 79), // Slowpoke
	EvolutionLines::Two(80, 81), // Magnemite
	EvolutionLines::One(82), // Farfetch'd
	EvolutionLines::Two(83, 84), // Doduo
	EvolutionLines::Two(85, 86), // Seel
	EvolutionLines::Two(87, 88), // Grimer
	EvolutionLines::Two(89, 90), // Shellder
	EvolutionLines::Three(91, 92, 93), // Gastly
	EvolutionLines::One(94), // Onix
	EvolutionLines::Two(95, 96), // Drowzee
	EvolutionLines::Two(97, 98), // Krabby
	EvolutionLines::Two(99, 100), // Voltorb
	EvolutionLines::Two(101, 102), // Exeggcute
	EvolutionLines::Two(103, 104), // Cubone
	EvolutionLines::One(105), // Hitmonlee
	EvolutionLines::One(106), // Hitmonchan
	EvolutionLines::One(107), // Lickitung
	EvolutionLines::Two(108, 109), // Koffing
	EvolutionLines::Two(110, 111), // Rhyhorn
	EvolutionLines::One(112), // Chansey
	EvolutionLines::One(113), // Tangela
	EvolutionLines::One(114), // Kangaskhan
	EvolutionLines::Two(115, 116), // Horsea
	EvolutionLines::Two(117, 118), // Goldeen
	EvolutionLines::Two(119, 120), // Staryu
	EvolutionLines::One(121), // Mr. Mime
	EvolutionLines::One(122), // Scyther
	EvolutionLines::One(123), // Jynx
	EvolutionLines::One(124), // Electabuzz
	EvolutionLines::One(125), // Magmar
	EvolutionLines::One(126), // Pinsir
	EvolutionLines::One(127), // Tauros
	EvolutionLines::Two(128, 129), // Magikarp
	EvolutionLines::One(130), // Lapras
	EvolutionLines::One(131), // Ditto
	EvolutionLines::Eevee(132, 133, 134, 135), // Eevee
	EvolutionLines::One(136), // Porygon
	EvolutionLines::Two(137, 138), // Omanyte
	EvolutionLines::Two(139, 140), // Kabuto
	EvolutionLines::One(141), // Aerodactyl
	EvolutionLines::One(142), // Snorlax
	EvolutionLines::One(143), // Articuno
	EvolutionLines::One(144), // Zapdos
	EvolutionLines::One(145), // Moltres
	EvolutionLines::Three(146, 147, 148), // Dratini
	EvolutionLines::One(149), // Mewtwo
	EvolutionLines::One(150), // Mew
];
	

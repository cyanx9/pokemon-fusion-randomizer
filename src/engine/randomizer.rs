use std::path::Path;
use std::fs::File;
use crate::engine::rom_manager::*;
use crate::engine::constants::*;
use crate::engine::log::*;
use crate::engine::pokemon::*;
use druid::Data;
use rand::Rng;
use rand::prelude::*;
use std::fs::OpenOptions;
use std::io::Write;

#[derive(Clone, Data, PartialEq)]
pub enum RandomizerAlgorithm {
	Random,
	Evo,
	Reverse,
}

pub fn randomize(rom_path: &Path,
				 log_path: &Path, log_on: &bool,
				 save_path: &Path,
				 shuffle_on: &bool, algorithm: &RandomizerAlgorithm) {
	
	match std::fs::copy(rom_path, save_path) {
		Err(why) => panic!("Could not copy file: {}", why),
		Ok(_) => (),
	}
	
	let display_rom = rom_path.display();
	let mut rom = match File::open(&rom_path) {
		Err(why) => panic!("Could not open {}: {}", display_rom, why),
		Ok(rom) => rom,
	};
	
	let display_randomized_rom = save_path.display();
	let randomized_rom = match OpenOptions::new().write(true).append(false).open(&save_path) {
		Err(why) => panic!("Could not open {}: {}", display_randomized_rom, why),
		Ok(rom) => rom,
	};
	
	let names = match read_names(&mut rom) {
		Err(why) => panic!("Reading names from rom failed! {}", why),
		Ok(names) => names,
	};

	let base_data = match read_base_data(&mut rom) {
		Err(why) => panic!("Reading base data from rom failed: {}", why),
		Ok(data) => data,
	};

	let palette_data = match read_palette_data(&mut rom) {
		Err(why) => panic!("Reading palette data from rom failed: {}", why),
		Ok(data) => data,
	};

	let cry_data = match read_cry_data(&mut rom) {
		Err(why) => panic!("Reading cry data from rom failed: {}", why),
		Ok(data) => data,
	};

	let pokemon = create_pokemon(names, base_data, palette_data, cry_data);
	let fusion_index = create_fusion_index(&shuffle_on, &algorithm);

	let fusion_pokemon = fuse_all(&pokemon, &fusion_index);

	let randomized_names = extract_names(&fusion_pokemon);
	let randomized_palettes = extract_palettes(&fusion_pokemon);
	let randomized_base_data = extract_base_data_no_mew(&fusion_pokemon);
	let fusion_numbers = extract_fusion_numbers(&fusion_index);
	write_names(&randomized_rom, &randomized_names);
	write_palettes(&randomized_rom, &randomized_palettes);
	write_base_data(&randomized_rom, &randomized_base_data, &fusion_pokemon[150].get_base_data());
	write_fusion_numbers(&randomized_rom, &fusion_numbers);
	if *log_on {
		let log = create_log(&pokemon, &fusion_pokemon, &fusion_index);
		let display_log = log_path.display();
		let mut log_file = match File::create(&log_path) {
			Err(why) => panic!("Could not open {}: {}", display_log, why),
			Ok(log) => log,
		};
		match write!(&mut log_file, "{}", log) {
			Err(why) => panic!("Could not write {}: {}", display_log, why),
			Ok(log) => log,
		};
	}
}

fn create_fusion_index(shuffle_on: &bool, algorithm: &RandomizerAlgorithm) -> Vec<usize> {
	let mut result = vec![0;RB_POKENUM];
	let mut rng = rand::thread_rng();
	match algorithm {
		RandomizerAlgorithm::Random => {
			match shuffle_on {
				true => {
					for i in 0..RB_POKENUM {
						result[i] = i;
					}
					result.shuffle(&mut rng);
					return result;
				},
				false => {
					for i in 0..RB_POKENUM {
						result[i] = rng.gen_range(0..RB_POKENUM);
					}
					return result;
				},
			}
		},
		RandomizerAlgorithm::Evo => {
			let evo_lines_original = EVOLUTION_LINES.clone();
			let evo_lines = match shuffle_on {
				true => {
					let mut r = EVOLUTION_LINES.clone();
					r.shuffle(&mut rng);
					r
				},
				false => {
					let mut r = EVOLUTION_LINES.clone();
					for i in 0..RB_EVO_LINES_NUMBER {
						r[i] = EVOLUTION_LINES[rng.gen_range(0..RB_EVO_LINES_NUMBER)].clone();
					}
					r
				}
			};
			let mut i = 0;
			for x in evo_lines_original {
						match x {
							EvolutionLines::One(index) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index] = s_index;
									},
									EvolutionLines::Two(_s_index1, s_index2) => {
										result[index] = s_index2;
									},
									EvolutionLines::Three(_s_index1, _s_index2, s_index3) => {
										result[index] = s_index3;
									},
									EvolutionLines::Eevee(_s_index1, s_index2, s_index3, s_index4) => {
										result[index] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
									},
								}
							},
							EvolutionLines::Two(index1, index2) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index1] = s_index;
										result[index2] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index1] = s_index1;
										result[index2] = s_index2;
									},
									EvolutionLines::Three(s_index1, _s_index2, s_index3) => {
										result[index1] = s_index1;
										result[index2] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index1] = s_index1;
										result[index2] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
									},
								}
							},
							EvolutionLines::Three(index1, index2, index3) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index1] = s_index;
										result[index2] = s_index;
										result[index3] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index1] = s_index1;
										result[index2] = s_index2;
										result[index3] = s_index2;
									},
									EvolutionLines::Three(s_index1, s_index2, s_index3) => {
										result[index1] = s_index1;
										result[index2] = s_index2;
										result[index3] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index1] = s_index1;
										result[index2] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
										result[index3] = result[index2].clone();
									},
								}
							},
							EvolutionLines::Eevee(index1, index2, index3, index4) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index1] = s_index;
										result[index2] = s_index;
										result[index3] = s_index;
										result[index4] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index1] = s_index1;
										result[index2] = s_index2;
										result[index3] = s_index2;
										result[index4] = s_index2;
									},
									EvolutionLines::Three(s_index1, _s_index2, s_index3) => {
										result[index1] = s_index1;
										result[index2] = s_index3;
										result[index3] = s_index3;
										result[index4] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index1] = s_index1;
										result[index2] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
										result[index3] = result[index2].clone();
										result[index4] = result[index2].clone();
									},
								}
							},
						}
				i = i+1;
			}
			return result;
		},
		RandomizerAlgorithm::Reverse => {
			let evo_lines_original = EVOLUTION_LINES.clone();
			let evo_lines = match shuffle_on {
				true => {
					let mut r = EVOLUTION_LINES.clone();
					r.shuffle(&mut rng);
					r
				},
				false => {
					let mut r = EVOLUTION_LINES.clone();
					for i in 0..RB_EVO_LINES_NUMBER {
						r[i] = EVOLUTION_LINES[rng.gen_range(0..RB_EVO_LINES_NUMBER)].clone();
					}
					r
				}
			};
			let mut i = 0;
			for x in evo_lines_original {
						match x {
							EvolutionLines::One(index) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index] = s_index;
									},
									EvolutionLines::Two(s_index1, _s_index2) => {
										result[index] = s_index1;
									},
									EvolutionLines::Three(s_index1, _s_index2, _s_index3) => {
										result[index] = s_index1;
									},
									EvolutionLines::Eevee(s_index1, _s_index2, _s_index3, _s_index4) => {
										result[index] = s_index1;
									},
								}
							},
							EvolutionLines::Two(index1, index2) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index2] = s_index;
										result[index1] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index2] = s_index1;
										result[index1] = s_index2;
									},
									EvolutionLines::Three(s_index1, _s_index2, s_index3) => {
										result[index2] = s_index1;
										result[index1] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index2] = s_index1;
										result[index1] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
									},
								}
							},
							EvolutionLines::Three(index1, index2, index3) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index3] = s_index;
										result[index2] = s_index;
										result[index1] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index3] = s_index1;
										result[index2] = s_index2;
										result[index1] = s_index2;
									},
									EvolutionLines::Three(s_index1, s_index2, s_index3) => {
										result[index3] = s_index1;
										result[index2] = s_index2;
										result[index1] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index3] = s_index1;
										result[index2] = match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
										result[index1] = result[index2].clone();
									},
								}
							},
							EvolutionLines::Eevee(index1, index2, index3, index4) => {
								match evo_lines[i] {
									EvolutionLines::One(s_index) => {
										result[index4] = s_index;
										result[index3] = s_index;
										result[index2] = s_index;
										result[index1] = s_index;
									},
									EvolutionLines::Two(s_index1, s_index2) => {
										result[index4] = s_index1;
										result[index3] = s_index1;
										result[index2] = s_index1;
										result[index1] = s_index2;
									},
									EvolutionLines::Three(s_index1, _s_index2, s_index3) => {
										result[index4] = s_index1;
										result[index3] = s_index1;
										result[index2] = s_index1;
										result[index1] = s_index3;
									}
									EvolutionLines::Eevee(s_index1, s_index2, s_index3, s_index4) => {
										result[index4] = s_index1;
										result[index3] = s_index1;
										result[index2] = s_index1;
										result[index1] =  match rng.gen_range(0..3) {
											0 => s_index2,
											1 => s_index3,
											2 => s_index4,
											_ => panic!("should not happen"),
										};
									},
								}
							},
						}
				i = i+1;
			}
			return result;
		},
	}
}

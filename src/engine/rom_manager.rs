use std::io;
use std::fs::File;
use std::io::SeekFrom;
use std::io::prelude::*;
use crate::engine::constants::*;

pub fn read_names(mut file: &File) -> io::Result<[u8;RB_INDEXNUM*RB_NAME_LENGTH]> {
	let mut names = [0;RB_INDEXNUM*RB_NAME_LENGTH];
	match file.seek(SeekFrom::Start(RB_NAMES.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_NAMES.try_into().unwrap()),
	};
	match file.read(&mut names) {
		Err(why) => panic!("Could not read! {}",  why),
		Ok(n) => assert_eq!(n, RB_INDEXNUM*RB_NAME_LENGTH),
	};
	
	Ok(names)
}

pub fn read_base_data(mut file: &File) -> io::Result<[u8;RB_POKENUM*RB_BASE_DATA_LENGTH]> {
	let mut stats150 = [0;(RB_POKENUM-1)*RB_BASE_DATA_LENGTH];
	match file.seek(SeekFrom::Start(RB_BASE_DATA.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA.try_into().unwrap()),
	};
	match file.read(&mut stats150) {
		Err(why) => panic!("Could not read! {}",  why),
		Ok(n) => assert_eq!(n, (RB_POKENUM-1)*RB_BASE_DATA_LENGTH),
	};
	
	let mut stats_mew = [0;RB_BASE_DATA_LENGTH];
	match file.seek(SeekFrom::Start(RB_BASE_DATA_MEW.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA_MEW.try_into().unwrap()),
	};
	match file.read(&mut stats_mew) {
		Err(why) => panic!("Could not read! {}",  why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA_LENGTH),
	};
	
	let mut stats = [0;RB_POKENUM*RB_BASE_DATA_LENGTH];
	let mut index = 0;
	for x in stats150 {
		stats[index] = x;
		index = index+1;
	}
	index = 1;
	for x in stats_mew.iter().rev() {
		stats[stats.len()-index] = *x;
		index = index+1;
	}

	Ok(stats)
}

pub fn read_palette_data(mut file: &File) -> io::Result<[u8;RB_POKENUM]> {
	let mut palettes = [0;RB_POKENUM];
	match file.seek(SeekFrom::Start(RB_PALETTE_DATA.try_into().unwrap())) {
		Err(why) => panic!("Cannot seek! {}", why),
		Ok(n) => assert_eq!(n, RB_PALETTE_DATA.try_into().unwrap()),
	};
	match file.read(&mut palettes) {
		Err(why) => panic!("Cannot read! {}", why),
		Ok(n) => assert_eq!(n, RB_POKENUM),
	};

	Ok(palettes)
}

pub fn read_cry_data(mut file: &File) -> io::Result<[u8;RB_INDEXNUM*RB_CRY_DATA_LENGTH]> {
	let mut cries = [0;RB_INDEXNUM * RB_CRY_DATA_LENGTH];
	match file.seek(SeekFrom::Start(RB_CRY_DATA.try_into().unwrap())) {
		Err(why) => panic!("Cannot seek! {}", why),
		Ok(n) => assert_eq!(n, RB_CRY_DATA.try_into().unwrap()),
	};
	match file.read(&mut cries) {
		Err(why) => panic!("Cannot read! {}", why),
		Ok(n) => assert_eq!(n, RB_INDEXNUM * RB_CRY_DATA_LENGTH),
	};

	Ok(cries)
}

pub fn write_names(mut file: &File, names: &[u8]) {
	match file.seek(SeekFrom::Start(RB_NAMES.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_NAMES.try_into().unwrap()),
	};
	match file.write(names) {
		Err(why) => panic!("Could not write names! {}",  why),
		Ok(n) => assert_eq!(n, RB_INDEXNUM*RB_NAME_LENGTH),
	};
	file.flush().unwrap();
}

pub fn write_palettes(mut file: &File, palettes: &[u8]) {
	match file.seek(SeekFrom::Start(RB_PALETTE_DATA.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_PALETTE_DATA.try_into().unwrap()),
	};
	match file.write(palettes) {
		Err(why) => panic!("Could not write palettes! {}", why),
		Ok(n) => assert_eq!(n, RB_POKENUM),
	};
	file.flush().unwrap();
}

pub fn write_base_data(mut file: &File, base_data: &[u8], mew: &[u8]) {
	
	match file.seek(SeekFrom::Start(RB_BASE_DATA.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA.try_into().unwrap()),
	};
	match file.write(&base_data) {
		Err(why) => panic!("Could not write base data! {}",  why),
		Ok(n) => assert_eq!(n, (RB_POKENUM-1)*RB_BASE_DATA_LENGTH),
	};
	file.flush().unwrap();
	
	match file.seek(SeekFrom::Start(RB_BASE_DATA_MEW.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA_MEW.try_into().unwrap()),
	};
	match file.write(&mew) {
		Err(why) => panic!("Could not write mew base data! {}",  why),
		Ok(n) => assert_eq!(n, RB_BASE_DATA_LENGTH),
	};
	file.flush().unwrap();
}

pub fn write_fusion_numbers(mut file: &File, fusion_numbers: &[u8]) {
	match file.seek(SeekFrom::Start(RB_FUSION_MONS.try_into().unwrap())) {
		Err(why) => panic!("Could not seek! {}", why),
		Ok(n) => assert_eq!(n, RB_FUSION_MONS.try_into().unwrap()),
	}
	match file.write(&fusion_numbers) {
		Err(why) => panic!("Could not write fusion numbers! {}", why),
		Ok(n) => assert_eq!(n, RB_INDEXNUM),
	}
	file.flush().unwrap();
}

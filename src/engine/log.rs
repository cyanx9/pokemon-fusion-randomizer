use crate::engine::pokemon::*;
use crate::engine::constants::*;


pub fn name_decoder(name: &[u8]) -> String {
	let mut name_decoded = String::new();
	for x in name {
		name_decoded
			.push(
				match x {
					0x80 => 'A',
					0x81 => 'B',
					0x82 => 'C',
					0x83 => 'D',
					0x84 => 'E',
					0x85 => 'F',
					0x86 => 'G',
					0x87 => 'H',
					0x88 => 'I',
					0x89 => 'J',
					0x8A => 'K',
					0x8B => 'L',
					0x8C => 'M',
					0x8D => 'N',
					0x8E => 'O',
					0x8F => 'P',
					0x90 => 'Q',
					0x91 => 'R',
					0x92 => 'S',
					0x93 => 'T',
					0x94 => 'U',
					0x95 => 'V',
					0x96 => 'W',
					0x97 => 'X',
					0x98 => 'Y',
					0x99 => 'Z',
					0xE8 => '.',
					0xEF => '\u{2642}',
					0xF5 => '\u{2640}',
					0xE0 => '\'',
					0x50 => ' ',
					_ => panic!("Unknown character in name encoding! {}", x),
				});
	}
	name_decoded
}

pub fn move_decoder(_move_num: &u8) -> String {
	unimplemented!();
}

pub fn type_decoder(type_num: &u8) -> String {
	match type_num {
		0x00 => String::from("NORMAL"),
		0x01 => String::from("FIGHTING"),
		0x02 => String::from("FLYING"),
		0x03 => String::from("POISON"),
		0x04 => String::from("GROUND"),
		0x05 => String::from("ROCK"),
		0x06 => String::from("BIRD"), // Unused type, should never be seen in a log
		0x07 => String::from("BUG"),
		0x08 => String::from("GHOST"),
		0x14 => String::from("FIRE"),
		0x15 => String::from("WATER"),
		0x16 => String::from("GRASS"),
		0x17 => String::from("ELECTRIC"),
		0x18 => String::from("PSYCHIC"),
		0x19 => String::from("ICE"),
		0x1A => String::from("DRAGON"),
		_ => panic!("Error: Unknown type!"),
	}
}

pub fn create_log(pokemon: &Vec<Pokemon>, fusion_pokemon: &Vec<Pokemon>, fusion_index: &[usize]) -> String {
	let mut log = String::new();
	log.push_str("Log\n");
	for i in 0..RB_POKENUM {
		log.push_str(&name_decoder(&pokemon[i].get_name()));
		log.push_str(" + ");
		log.push_str(&name_decoder(&pokemon[fusion_index[i]].get_name()));
		log.push_str("\n");
		log.push_str(&name_decoder(&fusion_pokemon[i].get_name()));
		log.push_str("\n");
		let base_data = fusion_pokemon[i].get_base_data();
		log.push_str("Pokedex: ");
		log.push_str(&base_data[0].to_string());
		log.push_str("\n");
		log.push_str("Base Stats: ");
		log.push_str(&base_data[1].to_string());
		log.push_str("(HP) ");
		log.push_str(&base_data[2].to_string());
		log.push_str("(ATK) ");
		log.push_str(&base_data[3].to_string());
		log.push_str("(DEF) ");
		log.push_str(&base_data[4].to_string());
		log.push_str("(SPE) ");
		log.push_str(&base_data[5].to_string());
		log.push_str("(SPC)\n");
		log.push_str("Type: ");
		log.push_str(&type_decoder(&base_data[6]));
		log.push_str("/");
		log.push_str(&type_decoder(&base_data[7]));
		log.push_str("\n");
		log.push_str("Catch Rate: ");
		log.push_str(&base_data[8].to_string());
		log.push_str("\n");
		log.push_str("Base Exp Yield: ");
		log.push_str(&base_data[9].to_string());
		log.push_str("\n");
		log.push_str("Attacks at lvl 1: ");
		log.push_str(&base_data[15].to_string());
		log.push_str(" ");
		log.push_str(&base_data[16].to_string());
		log.push_str(" ");
		log.push_str(&base_data[17].to_string());
		log.push_str(" ");
		log.push_str(&base_data[18].to_string());
		log.push_str("\n");
		log.push_str("Growth Rate: ");
		log.push_str(&base_data[19].to_string());
		log.push_str("\n");
		// TM/HM Compatibility
		log.push_str("Color Palette: ");
		log.push_str(&fusion_pokemon[i].get_palette().to_string());
		log.push_str("\n");
		log.push_str("-----------------------\n");
	}
	
	log
}

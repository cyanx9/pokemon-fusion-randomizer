pub const RB_POKENUM: usize = 151;
pub const RB_INDEXNUM: usize = 190;

pub const RB_NAMES: usize = 0x1C21E;
pub const RB_NAME_LENGTH: usize = 10;

pub const RB_BASE_DATA: usize = 0x383DE;
pub const RB_BASE_DATA_MEW: usize = 0x0425B;
pub const RB_BASE_DATA_LENGTH: usize = 28;

pub const RB_PALETTE_DATA: usize = 0x725C9;

pub const RB_CRY_DATA: usize = 0x39446;
pub const RB_CRY_DATA_LENGTH: usize = 3;

pub const RB_FUSION_MONS: usize = 0x3BC4E;

pub const RB_EVO_LINES_NUMBER: usize = 79;

pub const RED_SHA256: &str = "c398e95086f14e3c7336d840cf26a2be864bcb92b44e04a33e83bd5dad56afb8";
pub const BLUE_SHA256: &str = "fb44fa36ba0aa58b92112cf30095802fad03c02bbd9387e4fc0d29682f665189";

pub mod engine{
	pub mod pokemon;
	pub mod rom_manager;
	pub mod constants;
	pub mod log;
	pub mod randomizer;
}

pub mod gui {
	pub mod data;
	pub mod view;
	pub mod delegate;
}

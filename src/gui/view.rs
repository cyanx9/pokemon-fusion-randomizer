use druid::{widget::{Checkbox, Flex, Button, Label, Either, SizedBox, RadioGroup}, Widget, WidgetExt};

use crate::gui::data::*;
use crate::engine::randomizer::*;

pub fn build_ui() -> impl Widget<AppState> {
	Flex::column()
		.with_flex_child(
			Flex::row()
				.with_flex_child(Button::new("Open").on_click(AppState::click_open), 1.)
				.with_flex_child(Checkbox::new("Save log").lens(AppState::log_on), 2.)
				.with_flex_child(Label::raw().lens(AppState::lbl), 3.),
			1.
		)
		.with_flex_child(
			Either::new(
				|data: &AppState, _env| data.is_red_blue(),
				Flex::column()
					.with_flex_child(Button::new("Randomize").on_click(AppState::click_randomize), 1.)
					.with_flex_child(Checkbox::new("Shuffle").lens(AppState::shuffle_on), 2.)
					.with_flex_child(RadioGroup::new(vec![("Random", RandomizerAlgorithm::Random), ("Evo",RandomizerAlgorithm::Evo), ("Reverse", RandomizerAlgorithm::Reverse)]).lens(AppState::algorithm), 3.),
				SizedBox::empty(),
			),
			2.
		)
}

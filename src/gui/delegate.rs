use druid::{AppDelegate, commands, Command, DelegateCtx, Env, Handled, Target};

use crate::gui::data::*;

pub struct Delegate;

impl AppDelegate<AppState> for Delegate {
	fn command(
		&mut self,
		ctx: &mut DelegateCtx,
		_target: Target,
		cmd: &Command,
		data: &mut AppState,
		_env: &Env,
	) -> Handled {
		if let Some(file_info) = cmd.get(commands::OPEN_FILE) {
			data.calc_rom_type(file_info.path());
			return Handled::Yes;
		}
		if let Some(file_info) = cmd.get(SAVE_LOG) {
			data.set_log_path(file_info.path());
			return Handled::Yes;
		}
		if let Some(file_info) = cmd.get(SAVE_ROM) {
			data.set_save_path(file_info.path());
			ctx.submit_command(RUN_RANDOMIZER);
			return Handled::Yes;
		}
		if let Some(_) = cmd.get(RUN_RANDOMIZER) {
			println!("RUN RANDOMIZER");
			data.start_randomizer();
			return Handled::Yes;
		}
		Handled::No
	}
}
					

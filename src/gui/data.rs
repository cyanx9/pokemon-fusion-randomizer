use druid::{Data, Lens, EventCtx, Env, FileSpec, FileDialogOptions, Selector, FileInfo};
use std::path::Path;
use std::fs::File;
use std::io::Read;
use ring::digest::{Context, Digest, SHA256};
use crate::engine::constants::*;
use crate::engine::randomizer::*;
use data_encoding::HEXLOWER;

pub const SAVE_LOG: Selector<FileInfo> = Selector::new("SAVE_LOG");
pub const SAVE_ROM: Selector<FileInfo> = Selector::new("SAVE_ROM");
pub const RUN_RANDOMIZER: Selector = Selector::new("RUN");

#[derive(Clone, Data, Lens)]
pub struct AppState {
	lbl: String,
	
	rom_type: i8,
	rom_path: String,
	
	save_path: String,
	
	log_path: String,
	log_on: bool,

	shuffle_on: bool,
	algorithm: RandomizerAlgorithm,
}

impl AppState {
	pub fn new() -> Self {
		Self {
			lbl: String::from("Open a ROM to show randomizer options"),
			
			rom_type: 0,
			rom_path: String::from("No input ROM selected"),
			
			save_path: String::from("No output ROM selected"),
			
			log_path: String::from("No log output selected"),
			log_on: true,

			shuffle_on: true,
			algorithm: RandomizerAlgorithm::Evo,
		}
	}

	pub fn set_log_path(&mut self, path: &Path) {
		self.log_path = path.display().to_string();
	}

	pub fn set_save_path(&mut self, path: &Path) {
		self.save_path = path.display().to_string();
	}

	pub fn start_randomizer(&self) {
		randomize(Path::new(&self.rom_path),
				  Path::new(&self.log_path), &self.log_on,
				  Path::new(&self.save_path),
				  &self.shuffle_on, &self.algorithm);
	}

	pub fn is_red_blue(&self) -> bool {
		self.rom_type == 1
	}

	pub fn click_open(ctx: &mut EventCtx, _data: &mut Self, _env: &Env) {
		let gbc = FileSpec::new("GBC game", &["gbc"]);
		let open_dialog_options = FileDialogOptions::new()
			.allowed_types(vec![gbc])
			.default_type(gbc)
			.default_name("pokered.gbc")
			.name_label("ROM")
			.title("Open ROM to randomize")
			.button_text("Open");
		ctx.submit_command(druid::commands::SHOW_OPEN_PANEL.with(open_dialog_options));
	}

	pub fn click_randomize(ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
		if data.log_on {
			let log = FileSpec::new("Log file", &["txt"]);
			let log_dialog_options = FileDialogOptions::new()
			.allowed_types(vec![log])
			.default_type(log)
			.default_name("pokefusion.log")
			.name_label("LOG")
			.title("Choose where to save log")
			.button_text("Save")
			.accept_command(SAVE_LOG);
			ctx.submit_command(druid::commands::SHOW_SAVE_PANEL.with(log_dialog_options));
		}
		let gbc = FileSpec::new("GBC game", &["gbc"]);
		let save_dialog_options = FileDialogOptions::new()
			.allowed_types(vec![gbc])
			.default_type(gbc)
			.default_name("pokefusion.gbc")
			.name_label("SAVE")
			.title("Choose where to save ROM")
			.button_text("Save")
			.accept_command(SAVE_ROM);
		ctx.submit_command(druid::commands::SHOW_SAVE_PANEL.with(save_dialog_options));
	}

	pub fn calc_rom_type(&mut self, path: &Path) {
		let display = path.display();
		let mut file = match File::open(&path) {
			Err(why) => panic!("Could not open file {}: {}", display, why),
			Ok(file) => file,
		};
		let checksum = HEXLOWER.encode(sha256_digest(&mut file).as_ref());
		if checksum == RED_SHA256 || checksum == BLUE_SHA256 {
			self.lbl = display.to_string();
			self.rom_type = 1;
			self.rom_path = display.to_string();
		} else {
			self.lbl = String::from("File not valid");
			self.rom_type = 0;
			self.rom_path = String::from("No input ROM selected");
		}
	}
	
}

fn sha256_digest(mut file: &File) -> Digest {
	let mut context = Context::new(&SHA256);
	let mut buffer = [0; 1024];
	loop {
		let count = match file.read(&mut buffer) {
			Ok(n) => n,
			Err(why) => panic!("Error reading file: {}", why),
		};
		if count == 0 {
			break;
		}
		context.update(&buffer[..count]);
	}
	context.finish()
}

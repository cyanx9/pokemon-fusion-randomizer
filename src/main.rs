use druid::{AppLauncher, WindowDesc};

use pokemon_fusion_randomizer::gui::data::AppState;

use pokemon_fusion_randomizer::gui::view::build_ui;

use pokemon_fusion_randomizer::gui::delegate::*;

pub fn main() {
	let main_window = WindowDesc::new(build_ui)
		.title("Pokemon Fusion Randomizer")
		.window_size((400.0, 400.0));

	let initial_state = AppState::new();

	AppLauncher::with_window(main_window)
		.delegate(Delegate)
		.launch(initial_state)
		.expect("Failed to launch application");
}
